#!/bin/bash

echo "Criando serviços no cluster kubernetes..."

kubectl apply -f ./services.yml

echo "Criando deployments..."

kubectl apply -f ./deployment.yml